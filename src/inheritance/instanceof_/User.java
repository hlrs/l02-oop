package inheritance.instanceof_;

/**
 * @author Vyacheslav Gorbatykh
 * @since 10.10.2017
 */
public class User {
    public static void main(String[] args) {
Article article1 = new Article();
System.out.println("Article article1 = new Article();");
System.out.println("article1 instanceof Article = "+ (article1 instanceof Article));
System.out.println("article1 instanceof Book = " + (article1 instanceof Book));

Article article2 = new Book();
System.out.println("Article article2 = new Book();");
System.out.println("article2 instanceof Article = " + (article2 instanceof Article));
System.out.println("article2 instanceof Book = " + (article2 instanceof Book));

Book book = new Book();
System.out.println("Book book = new Book();");
System.out.println("book instanceof Article = " + (book instanceof Article));
System.out.println("book instanceof Book = " + (book instanceof Book));
    }
}
