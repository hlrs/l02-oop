package inheritance.simple;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class IncrementHolder extends Holder {
    public int getValue() {
        value++;
        return value;
    }
}
