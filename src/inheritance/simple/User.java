package inheritance.simple;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class User {
    public static void main(String[] args) {
        Holder holder = new Holder();
        holder.setValue(10);
        System.out.println("{" + holder.getValue() + "}");

        IncrementHolder incrementHolder = new IncrementHolder();
        incrementHolder.setValue(10);
        System.out.println("{" + incrementHolder.getValue() + "}");
    }
}
