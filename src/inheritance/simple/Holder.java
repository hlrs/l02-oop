package inheritance.simple;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Holder {
    protected int value;

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
