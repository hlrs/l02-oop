package inheritance.encoder;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.07.2017
 */
public class User {
    public static void main(String[] args) throws Exception {
        Encoder encoder = new Encoder();
        byte[] a1 = encoder.encode("Hello, world!");
        printArray(a1);

        LengthEncoder lengthEncoder = new LengthEncoder();
        byte[] a2 = lengthEncoder.encode("Hello, world!");
        printArray(a2);
    }

    public static void printArray(byte[] array) {
        String result = "{";
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                result += ", ";
            }
            result += array[i];
        }
        result += "}";

        System.out.println(result);
    }
}
