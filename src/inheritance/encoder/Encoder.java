package inheritance.encoder;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.07.2017
 */
public class Encoder {
    public byte[] encode(String message) throws Exception {
        return message.getBytes("UTF-8");
    }
}
