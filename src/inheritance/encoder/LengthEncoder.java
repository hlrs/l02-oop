package inheritance.encoder;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.07.2017
 */
public class LengthEncoder extends Encoder {
    public byte[] encode(String message) throws Exception {
        byte[] encodedMessage = super.encode(message);

        byte[] result = new byte[4 + encodedMessage.length];

        result[0] = (byte) (encodedMessage.length >> 24);
        result[1] = (byte) (encodedMessage.length >> 16);
        result[2] = (byte) (encodedMessage.length >> 8);
        result[3] = (byte) encodedMessage.length;
        System.arraycopy(encodedMessage, 0, result, 4, encodedMessage.length);

        return result;
    }
}
