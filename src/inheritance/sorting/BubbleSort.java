package inheritance.sorting;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class BubbleSort extends BaseSort {
    protected void applySort() {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }
}
