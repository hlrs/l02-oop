package inheritance.sorting;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public abstract class BaseSort {
    protected int[] array;

    public void setArray(int[] array) {
        this.array = array;
    }

    public void sort() {
        printArray();
        applySort();
        printArray();
    }

    protected abstract void applySort();

    private void printArray() {
        String result = "{";
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                result += ", ";
            }
            result += array[i];
        }
        result += "}";

        System.out.println(result);
    }
}
