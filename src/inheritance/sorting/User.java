package inheritance.sorting;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class User {
    public static void test(BaseSort sort) {
        System.out.println(sort.getClass().getSimpleName());

        int[] array = new int[]{5, 3, 89, 7, 42, 76, 11, 1};
        sort.setArray(array);
        sort.sort();
    }

    public static void main(String[] args) {
        NoSort noSort = new NoSort();
        test(noSort);

        BubbleSort bubbleSort = new BubbleSort();
        test(bubbleSort);
    }
}
