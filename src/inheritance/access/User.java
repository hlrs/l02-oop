package inheritance.access;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.07.2017
 */
public class User {
    public static void main(String[] args) {
        Auto auto;

        auto = new Auto();
        auto.test();

        auto = new Hybrid();
        auto.test();
    }
}
