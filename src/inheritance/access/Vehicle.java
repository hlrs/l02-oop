package inheritance.access;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.07.2017
 */
public class Vehicle {
    public void move(){
        System.out.println("Vehicle.move()");
    }
}
