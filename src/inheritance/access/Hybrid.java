package inheritance.access;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.07.2017
 */
public class Hybrid extends Auto {
    public void move() {
        System.out.println("Hybrid.move()");
    }
}
