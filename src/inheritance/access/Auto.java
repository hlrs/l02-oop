package inheritance.access;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.07.2017
 */
public class Auto extends Vehicle {
    public void move() {
        System.out.println("Auto.move()");
    }

    public void test() {
        System.out.println(this.getClass().getSimpleName());
        this.move();
        super.move();
        move();
    }
}
