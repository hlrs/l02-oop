package inheritance.super_;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.07.2017
 */
public class User {
    public static void main(String[] args) {

        int[] payments = new int[]{39, 15, 4, 42};

        InvoiceCalculator invoiceCalculator = new InvoiceCalculator();
        double p1 = invoiceCalculator.calculate(payments);
        System.out.println("{" + p1 + "}");

        TaxInvoiceCalculator taxInvoiceCalculator = new TaxInvoiceCalculator(0.07);
        double p2 = taxInvoiceCalculator.calculate(payments);
        System.out.println("{" + p2 + "}");
    }
}
