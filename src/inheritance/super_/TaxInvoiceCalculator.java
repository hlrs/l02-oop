package inheritance.super_;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.07.2017
 */
public class TaxInvoiceCalculator extends InvoiceCalculator {
    private double percent;

    public TaxInvoiceCalculator(double percent) {
        this.percent = percent;
    }

    public double calculate(int[] values) {
        double result = super.calculate(values);
        result += result * percent;
        return result;
    }
}
