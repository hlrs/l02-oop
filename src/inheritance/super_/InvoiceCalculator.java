package inheritance.super_;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.07.2017
 */
public class InvoiceCalculator {
    public double calculate(int[] values) {
        int sum = 0;
        for (int i = 0; i < values.length; i++) {
            sum += values[i];
        }
        return sum;
    }
}
