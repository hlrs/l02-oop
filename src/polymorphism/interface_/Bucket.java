package polymorphism.interface_;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.07.2017
 */
public class Bucket implements Figure {
    private int radius;
    private int height;

    public Bucket(int radius, int height) {
        this.radius = radius;
        this.height = height;
    }

    public double getVolume() {
        return 2 * Math.PI * radius * height;
    }
}
