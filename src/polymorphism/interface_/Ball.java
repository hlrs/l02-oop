package polymorphism.interface_;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Ball implements Figure {
    private int radius;

    public Ball(int radius) {
        this.radius = radius;
    }

    public double getVolume() {
        return 4.0 / 3.0 * Math.PI * radius * radius * radius;
    }
}
