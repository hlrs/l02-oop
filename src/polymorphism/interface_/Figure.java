package polymorphism.interface_;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public interface Figure {
    double getVolume();
}
