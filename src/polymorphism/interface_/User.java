package polymorphism.interface_;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class User {
    public static void test(Figure[] figures) {
        double sum = 0;
        for (Figure figure : figures) {
            sum += figure.getVolume();
        }
        System.out.println("{" + sum + "}");
    }

    public static void main(String[] args) {
        Figure[] figures = new Figure[3];
        figures[0] = new Ball(3);
        figures[1] = new Bucket(2, 2);
        figures[2] = new Country(25.7);

        test(figures);
    }
}
