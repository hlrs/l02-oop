package polymorphism.interface_;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.07.2017
 */
public class Country implements Figure {
    private double area;

    public Country(double area) {
        this.area = area;
    }

    public double getVolume() {
        return area;
    }
}
