package polymorphism.clothe;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Clothe {
    private int size;

    public void setSize(int size) {
        if (size >= getMinimumSize() && size <= getMaximumSize()) {
            this.size = size;
        }
    }

    public int getMinimumSize() {
        return 0;
    }

    public int getMaximumSize() {
        return Integer.MAX_VALUE;
    }

    public void printSize() {
        System.out.println("{" + size + "}");
    }
}
