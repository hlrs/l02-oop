package polymorphism.clothe;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class User {
    public static void test(Clothe clothe) {
        System.out.println(clothe.getClass().getSimpleName());

        clothe.printSize();
        clothe.setSize(100);
        clothe.printSize();
    }

    public static void main(String[] args) {
        Clothe clothe;

        clothe = new Clothe();
        test(clothe);

        clothe = new Shoe();
        test(clothe);
    }
}
