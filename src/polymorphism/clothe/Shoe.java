package polymorphism.clothe;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Shoe extends Clothe {
    public int getMinimumSize() {
        return 16;
    }

    public int getMaximumSize() {
        return 48;
    }
}
