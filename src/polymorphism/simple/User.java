package polymorphism.simple;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class User {
    public static void print(Printer printer) {
        printer.print("Username");
    }

    public static void main(String[] args) {
        Printer printer = new Printer();
        print(printer);

        HelloPrinter helloPrinter = new HelloPrinter();
        print(helloPrinter);

        TheBestPrinter theBestPrinter = new TheBestPrinter();
        print(theBestPrinter);
    }
}
