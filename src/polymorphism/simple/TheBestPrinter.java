package polymorphism.simple;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class TheBestPrinter extends Printer {
    public void print(String name) {
        System.out.println(name + " is the best!");
    }
}
