package polymorphism.simple;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class HelloPrinter extends Printer {
    public void print(String name) {
        super.print("Hello, " + name);
    }
}
