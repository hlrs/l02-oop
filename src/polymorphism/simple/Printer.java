package polymorphism.simple;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Printer {
    public void print(String name) {
        System.out.println(name);
    }
}
