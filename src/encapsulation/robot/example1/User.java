package encapsulation.robot.example1;

/**
 * No encapsulation example
 *
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class User {
    public static void main(String[] args) {
        Robot robot = new Robot();
        robot.printPosition();

        robot.x = 5;
        robot.y = 10;
        robot.printPosition();

        robot.x = 10;
        robot.y = 10;
        robot.printPosition();
    }
}
