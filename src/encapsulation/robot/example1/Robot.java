package encapsulation.robot.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Robot {
    public int x;
    public int y;

    public void printPosition() {
        System.out.println("{" + x + ", " + y + "}");
    }
}
