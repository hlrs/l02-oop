package encapsulation.robot.example2;

/**
 * No encapsulation example
 *
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class User {
    public static void main(String[] args) {
        Robot robot = new Robot();
        robot.printPosition();

        robot.setPosition(5, 10);
        robot.printPosition();

        robot.step(5, 0);
        robot.printPosition();
    }
}
