package encapsulation.robot.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Robot {
    private int x;
    private int y;

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void step(int dx, int dy) {
        x += dx;
        y += dy;
    }

    public void printPosition() {
        System.out.println("{" + x + ", " + y + "}");
    }
}
