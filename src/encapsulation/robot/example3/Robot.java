package encapsulation.robot.example3;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Robot {
    private int x;
    private int y;

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void step(Boolean left, Boolean down) {
        if (left != null) {
            x += left ? 1 : -1;
        }
        if (down != null) {
            y += down ? 1 : -1;
        }
    }

    public void printPosition() {
        System.out.println("{" + x + ", " + y + "}");
    }
}
