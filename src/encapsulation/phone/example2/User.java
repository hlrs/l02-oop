package encapsulation.phone.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class User {
    public static void main(String[] args) throws Exception {
        Phone phone = new Phone();
        phone.printAccumulator();

        phone.startCharge();
        Thread.sleep(100);
        phone.stopCharge();
        phone.printAccumulator();

        phone.startCharge();
        Thread.sleep(2000);
        phone.stopCharge();
        phone.printAccumulator();
    }
}
