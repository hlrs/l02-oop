package encapsulation.phone.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Phone {
    /**
     * 1% each 10 milliseconds
     */
    private static final long CHARGE_SPEED = 10;

    /**
     * In percent from 0% to 100%
     */
    private int accumulator;
    private long chargeStartTime;

    public void startCharge() {
        chargeStartTime = System.currentTimeMillis();
    }

    public void stopCharge() {
        long currentTime = System.currentTimeMillis();
        long chargedValue = (currentTime - chargeStartTime) / CHARGE_SPEED;

        accumulator += chargedValue;
        if (accumulator > 100) {
            accumulator = 100;
        }
    }

    public void printAccumulator() {
        System.out.println("{" + accumulator + "%}");
    }
}
