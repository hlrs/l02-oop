package encapsulation.phone.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class Phone {
    /**
     * In percent from 0% to 100%
     */
    private int accumulator;

    public void setAccumulator(int accumulator) {
        this.accumulator = accumulator;
    }

    public void printAccumulator() {
        System.out.println("{" + accumulator + "%}");
    }
}
