package encapsulation.phone.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.07.2017
 */
public class User {
    public static void main(String[] args) {
        Phone phone = new Phone();
        phone.printAccumulator();

        phone.setAccumulator(60);
        phone.printAccumulator();

        phone.setAccumulator(142);  //more then 100%
        phone.printAccumulator();
    }
}
